// Soal 1
var pertama = "saya sangat senang hari ini";
var car2 = pertama.substr(0, 5);
var car3 = pertama.substr(12, 7);

var kedua = "belajar javacript itu keren";
var str2 = kedua.substr(0, 7);

console.log(car2 + car3 + str2 + " JAVASCRIPT");

// Soal 2
var KataPertama = "10";
var KataKedua = "2";
var KataKetiga = "4";
var KataKeempat = "6";

var KataPertama = Number("10");
var KataKedua = Number("2");
var KataKetiga = Number("4");
var KataKeempat = Number("6");

console.log(KataPertama + KataKedua * KataKetiga + KataKeempat);

//Soal 3
var kalimat = "wah javascript itu keren sekali";

var KataPertama = kalimat.substring(0, 3);
var KataKedua = kalimat.substring(3, 14);
var KataKetiga = kalimat.substr(15, 3);
var KataKeempat = kalimat.substr(19, 5);
var KataKelima = kalimat.substr(25, 6);

console.log("Kata Pertama: " + KataPertama);
console.log("Kata kedua: " + KataKedua);
console.log("Kata ketiga: " + KataKetiga);
console.log("Kata keempat: " + KataKeempat);
console.log("Kata kelima: " + KataKelima);
