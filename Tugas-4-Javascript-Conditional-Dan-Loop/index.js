// Soal 1
var nilai = "83";

if (nilai >= 85) {
  console.log("Nilai A");
} else if ((nilai >= 75) & (nilai < 85)) {
  console.log("Nilai B");
} else if ((nilai >= 65) & (nilai < 75)) {
  console.log("Nilai C");
} else if ((nilai >= 55) & (nilai < 65)) {
  console.log("Nilai D");
} else if (nilai < 55) {
  console.log("Nilai E");
}

// Soal 2
var tanggal = 17;
var bulan = 5;
var tahun = 2002;
switch (bulan) {
  case 1: {
    console.log(tanggal + " Januari " + tahun);
    break;
  }
  case 2: {
    console.log(tanggal + " Febuari " + tahun);
    break;
  }
  case 3: {
    console.log(tanggal + " Maret " + tahun);
    break;
  }
  case 4: {
    console.log(tanggal + " April " + tahun);
    break;
  }
  case 5: {
    console.log(tanggal + " Mei " + tahun);
    break;
  }
  case 6: {
    console.log(tanggal + " Juni " + tahun);
    break;
  }
  case 7: {
    console.log(tanggal + " Juli " + tahun);
    break;
  }
  case 8: {
    console.log(tanggal + " Agustus " + tahun);
    break;
  }
  case 9: {
    console.log(tanggal + " September " + tahun);
    break;
  }
  case 10: {
    console.log(tanggal + " Oktober " + tahun);
    break;
  }
  case 11: {
    console.log(tanggal + " November " + tahun);
    break;
  }
  case 12: {
    console.log(tanggal + " Desember " + tahun);
    break;
  }
  default: {
    console.log("bulan Error");
  }
}

// Soal 3
var n = "";
for (var i = 0; i < 3; i++) {
  for (var j = 0; j <= i; j++) {
    n += "#";
  }
  n += "\n";
}
console.log(n);

var n = "";
for (var i = 0; i < 7; i++) {
  for (var j = 0; j <= i; j++) {
    n += "#";
  }
  n += "\n";
}
console.log(n);

// Soal 4
var m = 0;
for (var i = 0; i >= 10; i++) {
  m += i;
  console.log(m + " I love Programming");
}
