// Soal 1
const luas = (panjang, lebar) => {
  return panjang * lebar;
};

const kel = (panjang, lebar) => {
  return 2 * (panjang + lebar);
};

console.log(luas(5, 9));
console.log(kel(6, 23));

// Soal 2
const tampilNama = (first, last) => {
  return `${first} ${last}`;
};
console.log(tampilNama("William", "Imoh"));

// Soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};
const { firstName, lastName, address, hobby } = newObject;
console.log(firstName, lastName, address, hobby);

// Soal 4
let west = ["Will", "Chris", "Sam", "Holly"];
let east = ["Gill", "Brian", "Noel", "Maggie"];

let combined = [...west, ...east];
console.log(combined);

// Soal 5
const planet = "earth";
const view = "glass";
const before = `lorem ${view} dolor sit amet, consectetur elit, ${planet}`;

console.log(before);
